<?php declare(strict_types=1);


namespace Finsterforst\Cache;


use Finsterforst\Cache\Contract\Translator;

class CacheDataObjectTranslatorFactory
{
    public function translate(Translator $translator, Cache $cache) : array
    {
        return $translator::translate($cache->get()->data);
    }
}