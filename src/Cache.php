<?php declare(strict_types=1);


namespace Finsterforst\Cache;


class Cache
{
    const CACHE_DIRECTORY = 'cache';
    const CACHE_FILE_PREFIX = 'cache_';
    const CACHE_FILE_EXTENSION = '.json';

    private $filename;

    public function __construct(string $filename)
    {
        $this->filename = $this->buildCacheName($filename);
    }

    public function put($data, int $ttl = 600) : void
    {
        $this->create($ttl, $data);
    }

    protected function create(int $ttl = 0, $data = []) : void
    {
        $data = [
            'timestamp' => time(),
            'ttl' => $ttl,
            'data' => $data,
        ];
        $encoded = json_encode($data , JSON_PRETTY_PRINT);
        file_put_contents($this->getAbsolutePathToFile(), $encoded);
    }

    /**
     * @return \stdClass
     * @throws \Exception
     */
    public function get() : \stdClass
    {
        if (!$this->doesFileExists()) {
            $this->create();
        }

        $json = file_get_contents($this->getAbsolutePathToFile());
        $object = json_decode($json);

        if (!property_exists($object, 'timestamp')) {
            throw new \Exception('The cache file is missing the timestamp information.');
        }

        return $object;
    }

    public function doesFileExists() : bool
    {
        return file_exists($this->getAbsolutePathToFile());
    }

    /**
     * Whenever a new version of the cache file is needed, this method returns true. Else false.
     * @throws \Exception
     */
    public function checkIfUpdateIsNeeded() : bool
    {
        // Then + 28 days < now? Generate a new one!
        return $this->get()->timestamp + $this->get()->ttl < time() ? true : false;
    }

    public function delete()
    {
        if ($this->doesFileExists()) {
            unlink($this->getAbsolutePathToFile());
        }
    }

    protected function buildCacheName(string $filename) : string
    {
        return self::CACHE_FILE_PREFIX . $filename . self::CACHE_FILE_EXTENSION;
    }

    protected function getAbsolutePathToFile() : string
    {
        return __DIR__ . DIRECTORY_SEPARATOR . self::CACHE_DIRECTORY. DIRECTORY_SEPARATOR . $this->filename;
    }
}