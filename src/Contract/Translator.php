<?php declare(strict_types=1);


namespace Finsterforst\Cache\Contract;


interface Translator
{
    /**
     * @param \stdClass[] $data
     */
    public static function translate(array $data);
}