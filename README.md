# finsterforst\cache

All commands are based right from the root directory, if not otherwise stated. 

## Install

As project
```
git clone https://finsterforst@bitbucket.org/finsterforst/cache.git
cd cache
composer update
```

As library
```
composer config repositories.finsterforst/cache vcs https://finsterforst@bitbucket.org/finsterforst/cache.git
composer require finsterforst/cache
```


## Testing
```
cd tests
../vendor/bin/phpunit
```