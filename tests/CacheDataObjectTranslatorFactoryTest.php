<?php declare(strict_types=1);

namespace Finsterforst\Cache\Test;


use Finsterforst\Cache\Cache;
use Finsterforst\Cache\CacheDataObjectTranslatorFactory;
use Finsterforst\Cache\Test\CacheDataObjectTranslatorFactoryTest\MappingClass;

class CacheDataObjectTranslatorFactoryTest extends BaseTestClass
{
    public function testTranslatorFactoryMethodCallsTranslateMethod()
    {
        $file = $this->helperRegisterTestFile();

        $cache = new Cache($file->name);
        $cache->put([], 1000);

        $factory = new CacheDataObjectTranslatorFactory();
        $translated = $factory->translate(new MappingClass(), $cache);

        $this->assertArrayHasKey('test1', $translated);
        $this->assertEquals($translated['test1'], 'test2');
    }
}