<?php declare(strict_types=1);

namespace Finsterforst\Cache\Test;

use Finsterforst\Cache\Cache;


class CacheTest extends BaseTestClass
{
    /** @var stdClass[] */
    private $testFiles = [];

    public function tearDown(): void
    {
        foreach ($this->testFiles as $file) {
            if (file_exists($file->path)) {
                unlink($file->path);
            }
        }

        parent::tearDown();
    }

    public function helperRegisterTestFile($filename = 'testfile')
    {
        $object = new \stdClass();
        $object->name = $filename;
        $object->path = '../src/cache/cache_'.$filename.'.json';

        $this->testFiles[] = $object;

        return $object;
    }

    public function testPutCallsCreate()
    {
        $mock = $this->getMockBuilder(Cache::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['create'])
            ->getMock();

        $mock->expects($this->once())
            ->method('create')
            ->with(1, [])
            ->will($this->returnValue(null));

        $mock->put([], 1);
    }

    public function testCreateCreatesFile()
    {
        $file = $this->helperRegisterTestFile();

        $cache = new Cache($file->name);
        $cache->put([], 1);

        $this->assertTrue(file_exists($file->path));
    }

    public function testDoesGetCallsMethodDoesFileExists()
    {
        $file = $this->helperRegisterTestFile();

        $mock = $this->getMockBuilder(Cache::class)
            ->setConstructorArgs([$file->name])
            ->onlyMethods(['doesFileExists'])
            ->getMock();

        $mock->expects($this->once())->method('doesFileExists');

        $mock->get();
    }

    public function testDoesFileExistsReturnTrueWhenFileExist()
    {
        $file = $this->helperRegisterTestFile();

        $cache = new Cache($file->name);
        $cache->put([]);
        $this->assertTrue($cache->doesFileExists());
    }

    public function testDoesFileExistsReturnFalseWhenFileDoesNotExist()
    {
        $file = $this->helperRegisterTestFile();

        $cache = new Cache($file->name);

        $this->assertFalse($cache->doesFileExists());
    }

    public function testCheckIfUpdateIsNeededIsNeeded()
    {
        $file = $this->helperRegisterTestFile();

        $cache = new Cache($file->name);
        $cache->put([], -1);

        $this->assertTrue($cache->checkIfUpdateIsNeeded());
    }

    public function testCheckIfUpdateIsNeededIsNotNeeded()
    {
        $file = $this->helperRegisterTestFile();

        $cache = new Cache($file->name);
        $cache->put([], 1000);

        $this->assertFalse($cache->checkIfUpdateIsNeeded());
    }

    public function testGet()
    {
        $file = $this->helperRegisterTestFile();

        $cache = new Cache($file->name);
        $cache->put(['a' => 'b'], 1000);

        $cacheObject = $cache->get();

        $this->assertObjectHasAttribute('timestamp', $cacheObject);
        $this->assertObjectHasAttribute('ttl', $cacheObject);
        $this->assertObjectHasAttribute('data', $cacheObject);

        $this->assertTrue(time() >= $cacheObject->timestamp);
        $this->assertEquals(1000, $cacheObject->ttl);

        $this->assertObjectHasAttribute('a', $cacheObject->data);
        $this->assertEquals('b', $cacheObject->data->a);
    }

    public function testDelete()
    {
        $file = $this->helperRegisterTestFile();

        $cache = new Cache($file->name);
        $cache->put(['a' => 'b'], 1000);

        $this->assertTrue($cache->doesFileExists());

        $cache->delete();

        $this->assertFalse($cache->doesFileExists());
    }
}