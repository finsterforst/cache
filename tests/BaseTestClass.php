<?php declare(strict_types=1);


namespace Finsterforst\Cache\Test;


use PHPUnit\Framework\TestCase;

class BaseTestClass extends TestCase
{
    /** @var stdClass[] */
    private $testFiles = [];

    public function tearDown(): void
    {
        foreach ($this->testFiles as $file) {
            if (file_exists($file->path)) {
                unlink($file->path);
            }
        }

        parent::tearDown();
    }


    public function helperRegisterTestFile($filename = 'testfile')
    {
        $object = new \stdClass();
        $object->name = $filename;
        $object->path = '../src/cache/cache_'.$filename.'.json';

        $this->testFiles[] = $object;

        return $object;
    }
}