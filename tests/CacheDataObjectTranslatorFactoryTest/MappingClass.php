<?php declare(strict_types=1);


namespace Finsterforst\Cache\Test\CacheDataObjectTranslatorFactoryTest;


class MappingClass implements \Finsterforst\Cache\Contract\Translator
{
    public static function translate(array $data)
    {
        $data['test1'] = 'test2';
        return $data;
    }
}