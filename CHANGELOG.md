# Change Log for finsterforst\cache

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [1.0.0] - 2020-02-29

### Added
- Put
- Get
- TTL (default 10 minutes)
- Factory which accepts Finsterforst\Cache\Contract\Translator objects
- Tests